export default function (sequelize, DataTypes) {
  var UserLikes = sequelize.define('UserLikes', {
    userId: DataTypes.INTEGER,
    likedUserId: DataTypes.INTEGER
  }, {});
  UserLikes.associate = function (models) {
    UserLikes.belongsTo(models.User, { foreignKey: 'userId' });
  };
  return UserLikes;
}
import * as db from '../models/index';

export async function data(req, res, next) {
  try {
    let userInfo = await db.User.findOne({
        attributes: ['id', 'username'],
        where : {id: req.user.id}, 
        include: [{ 
          model: db.UserLikes, 
          attributes: ['likedUserId'],
          required: false 
        }]
      });
    if (userInfo) {
      let likeNumber = userInfo.UserLikes.length;
      return res.status(200).send({userInfo, likeNumber});
    } 
    throw 'User not found'
  } catch (err) {
    next(err);
  }
}

export async function updatePassword(req, res, next) {
  try {
    let user = await db.User.findOne({where: {id: req.user.id}});
    user.password = req.body.password;
    await user.save();
    res.status(200).send('done');
  } catch (err) {
    next(err);
  }

}

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import { signupUser } from '../actions/auth';

function mapStateToProps(state) {
  return { errorMessage: state.auth.error };
}

class Register extends Component {
  handleFormSubmit = (username, password) => {
    this.props.signupUser(username, password);
  }

  renderError() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong>Error!</strong>
          <br />
          {this.props.errorMessage}
        </div>
      );
    }
    return null;
  }

  renderUsernameField = field => (
    <input
      {...field.input}
      type="text"
      className="form-control"
      placeholder="Username"
      minLength="3"
      maxLength="20"
      required
    />
  );

  renderPasswordField = field => {
    const {
      meta: { touched, error },
    } = field;

    return (
      <div>
        <input
          {...field.input}
          type="password"
          className="form-control"
          minLength="5"
          maxLength="15"
          placeholder={field.placeholder}
          required
        />
        <div className="text-help">{touched ? error : ''}</div>
      </div>
    );
  };

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="starter-template">
        <form
          className="form-signin"
          onSubmit={handleSubmit(this.handleFormSubmit)}
        >
          <h1 className="h3 mb-3 font-weight-normal">Signup</h1>
          <Field name="username" component={this.renderUsernameField} />
          <Field
            name="password"
            component={this.renderPasswordField}
            placeholder="Password"
          />
          <Field
            name="passwordConfirm"
            component={this.renderPasswordField}
            placeholder="Password (confirm)"
          />
          <br />
          {this.renderError()}
          <button className="btn btn-lg btn-primary btn-block" type="submit">
            Signup
          </button>
        </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  if (values.password !== values.passwordConfirm) {
    errors.passwordConfirm =
      'Password do not match';
  }

  return errors;
}

export default reduxForm({
  validate,
  form: 'SignupForm',
})(
  connect(
    mapStateToProps,
    { signupUser },
  )(Register),
);

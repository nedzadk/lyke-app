import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getUsers, likeUser, unlikeUser } from '../actions/users';
import { checkExistingToken } from '../actions/auth';

class Home extends Component {
  componentDidMount() {
    this.props.checkExistingToken();
    this.props.getUsers();
  }

  onLikeClick = event => {
    this.props.likeUser(event.target.value);
  }

  onUnlikeClick = event => {
    this.props.unlikeUser(event.target.value);
  }

  renderLikeButton(user) {
    let likeBtn = null;
    if (this.props.authenticated && this.props.currentUser.username !== user.username ) {
      likeBtn = <button type="button"
                    className={`btn ${user.meLike ? 'btn-danger' : 'btn-success'} btn-block`}
                    value={user.id}
                    disabled={user.disabled}
                    onClick={user.meLike ? this.onUnlikeClick : this.onLikeClick}
                    >{user.meLike ? 'Unlike' : 'Like'}</button>;
    }
    return likeBtn;
  }

  renderUserRows() {
    const users = this.props.users.allUsers;
    const myLikes = this.props.currentUser.UserLikes;
    return users.map((user) => {
      if (myLikes) {
        let findLike = myLikes.find(like => like.likedUserId === user.id);
        if (findLike) {
          user.meLike = true;
        } else {
          user.meLike = false;
        }
      }
      return (
        <tr key={user.id}>
          <th scope="row">{user.id}</th>
          <td>{user.username}</td>
          <td>{user.likeCount}</td>
          <td>{this.renderLikeButton(user)}</td>
        </tr>
      );
    });
  }

  renderUsers() {
    const { error } = this.props.users;

    if (error) {
      return <div className="alert alert-danger">Error: {error.message}</div>
    }

    return (
      <table className="table">
        <thead>
          <tr>
            <th scope="col" style={{width:'20px'}}>ID</th>
            <th scope="col">username</th>
            <th scope="col">likes</th>
            <th scope="col" style={{width:'100px'}}></th>
          </tr>
        </thead>
        <tbody>
          {this.renderUserRows()}
        </tbody>
      </table>
    );
  }

  render() {
    const { loading } = this.props.users;
    if (loading) {
      return (
        <div>Getting users...</div>
      );
    }
    return (
        <div className="starter-template">
          <div className="row">
            <div className="col">
              <h1>Users</h1>
            </div>

          </div>
          {this.renderUsers()}
        </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    users: state.users,
    authenticated: state.auth.authenticated,
    currentUser: state.me.currentUser,
  };
}

export default connect(mapStateToProps, { getUsers, likeUser, unlikeUser, checkExistingToken })(Home);

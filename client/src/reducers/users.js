import {
  FETCH_USERS,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE,
  UPDATE_USER,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILURE,
  DISABLE_LIKE_BUTTON,
  ENABLE_LIKE_BUTTON,
} from '../constants/actionTypes';

const INIT_STATE = {
  allUsers: [],
  error: null,
  loading: true,
};

export default function(state = INIT_STATE, action) {
  let error;
  let idUser;

  switch (action.type) {
    case FETCH_USERS:
      return { ...state, allUsers: [], error: null, loading: true };
    case FETCH_USERS_SUCCESS:
      return {
        ...state,
        allUsers: action.payload,
        error: null,
        loading: false,
      };
    case FETCH_USERS_FAILURE:
      error = action.payload || { message: action.payload.message };
      return {
        ...state,
        allUsers: { users: [], error, loading: false },
      };

    case UPDATE_USER:
      return { ...state, error: null, loading: true };
    case UPDATE_USER_SUCCESS:
      return {
        ...state,
        allUsers: state.allUsers.map(
          old => (old.id === action.payload.id ? action.payload : old),
        ),
        error: null,
        loading: false,
      };
    case UPDATE_USER_FAILURE:
      error = action.payload || { message: action.payload.message };
      return {
        ...state,
        allUsers: { users: [], error, loading: false },
      };

    case DISABLE_LIKE_BUTTON:
      idUser = Number.parseInt(action.payload, 10);
      return {
        ...state,
        allUsers: state.allUsers.map(
          old => (old.id === idUser ? { ...old, disabled: true } : old),
        ),
      };
    case ENABLE_LIKE_BUTTON:
      idUser = Number.parseInt(action.payload, 10);
      return {
        ...state,
        allUsers: state.allUsers.map(
          old => (old.id === idUser ? { ...old, disabled: false } : old),
        ),
      };
    default:
      return state;
  }
}

import reducer from './me';
import { GET } from '../constants/actionTypes';

describe('Reducer: me', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      currentUser: {},
      error: null,
    });
  });

  it('should handle GET', () => {
    const payload = {
        username: 1,
    };

    expect(
      reducer(undefined, {
        type: GET,
        payload,
      }),
    ).toEqual({
      currentUser: {
        username: 1,
      },
      error: null,
    });
  });
});

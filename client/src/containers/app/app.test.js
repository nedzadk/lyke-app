import React from 'react';
import ReactDOM from 'react-dom';
import ReduxThunk from 'redux-thunk';
import ReduxPromise from 'redux-promise'
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';

import reducers from '../../reducers';

import App from './App';

const createStoreWithMiddleware = applyMiddleware(ReduxThunk, ReduxPromise)(createStore);
const store = createStoreWithMiddleware(reducers);

describe('App', () => {

  it('renders without crashing', () => {
    const div = document.createElement('root');
    ReactDOM.render(
      <Provider store={store}>
        <App/>
      </Provider>,
      div
    );

    ReactDOM.unmountComponentAtNode(div);
  });


})


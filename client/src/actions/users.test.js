import { DISABLE_LIKE_BUTTON, ENABLE_LIKE_BUTTON } from '../constants/actionTypes';
import * as actions from './users';

describe('Actions: users_actions', () => {
  it('should create an action to disableLikeButton', () => {
    const expectedAction = {
      type: DISABLE_LIKE_BUTTON,
      payload: 123,
    };
    expect(actions.disableLikeButton(123)).toEqual(expectedAction);
  });

  it('should create an action to enableLikeButton', () => {
    const expectedAction = {
      type: ENABLE_LIKE_BUTTON,
      payload: 123,
    };
    expect(actions.enableLikeButton(123)).toEqual(expectedAction);
  });
});
